package com.example.exercice3;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    // pour afficher l'interface en code java décommentez les codes si dessous
   /* LinearLayout ll;
    TextView tvTitre,tvNom,tvPrenom,tvAge,tvDomComp,tvNumTel;
    EditText edNom,edPrenom,edAge,edDomComp,edNumTel;
    Button btnValider;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      /*   ll=new LinearLayout(this);
         ll.setOrientation(LinearLayout.VERTICAL);

         // titre
        tvTitre=new TextView(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvTitre.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
        }
        tvTitre.setText(R.string.textView_titre);
        tvTitre.setPadding(100,5 ,0,0);
        ll.addView(tvTitre);

         //Textview nom
        tvNom=new TextView(this);
        tvNom.setText(R.string.textView_nom);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvNom.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
        }
        ll.addView(tvNom);

        //EditText nom
        edNom=new EditText(this);
        edNom.setHint(R.string.editText_nom);
        ll.addView(edNom);
        setContentView(ll);

        //Textview Prénom
        tvPrenom=new TextView(this);
        tvPrenom.setText(R.string.textView_prenom);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvPrenom.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
        }
        ll.addView(tvPrenom);

        //EditText prénom
        edPrenom=new EditText(this);
        edPrenom.setHint(R.string.editText_prenom);
        ll.addView(edPrenom);
        setContentView(ll);

        //TextView age
        tvAge=new TextView(this);
        tvAge.setText(R.string.textView_age);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvAge.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
        }
        ll.addView(tvAge);

        //EditText age
        edAge=new EditText(this);
        edAge.setHint(R.string.editText_age);
        edAge.setInputType(InputType.TYPE_CLASS_NUMBER);
        ll.addView(edAge);

        //Textview Domaine de compétence
        tvDomComp=new TextView(this);
        tvDomComp.setText(R.string.textView_dom_comp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvDomComp.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
        }
        ll.addView(tvDomComp);
        //EditText Domaine de compétence
        edDomComp=new EditText(this);
        edDomComp.setHint(R.string.editText_dom_comp);
        ll.addView(edDomComp);

        //TextView Numéro de téléphone
        tvNumTel=new TextView(this);
        tvNumTel.setText(R.string.textView_num_tel);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvNumTel.setTextAppearance(R.style.TextAppearance_AppCompat_Large);
        }
        ll.addView(tvNumTel);

        //EditText Numéro de téléphone
        edNumTel=new EditText(this);
        edNumTel.setHint(R.string.editText_age);
        edNumTel.setInputType(InputType.TYPE_CLASS_PHONE);
        ll.addView(edNumTel);

        // Bouton valider
        btnValider= new Button(this);
        btnValider.setText(R.string.btn_valider);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        btnValider.setLayoutParams(lp);
        btnValider.setBackgroundColor(Color.parseColor("#4b006e"));
        btnValider.setTextColor(Color.parseColor("#FFFFFFFF"));
        ll.addView(btnValider);

        setContentView(ll);*/



    }
}